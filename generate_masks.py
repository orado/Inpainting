import os, sys
import numpy as np
import cv2 as cv


def get_mask(frame1, frame2):

    f1 = cv.cvtColor(frame1,cv.COLOR_BGR2GRAY)
    hsv = np.zeros_like(frame1)
    hsv[...,1] = 255
    
    f2 = cv.cvtColor(frame2,cv.COLOR_BGR2GRAY)
    flow = cv.calcOpticalFlowFarneback(f1,f2, None, 0.5, 3, 15, 3, 5, 1.2, 0)
    mag, ang = cv.cartToPolar(flow[...,0], flow[...,1])
    hsv[...,0] = ang*180/np.pi/2
    hsv[...,2] = cv.normalize(mag,None,0,255,cv.NORM_MINMAX)
    bgr = cv.cvtColor(hsv,cv.COLOR_HSV2BGR)
    gray = cv.cvtColor(bgr,cv.COLOR_BGR2GRAY)

    #ret,thresh1 = cv2.threshold(img,127,255,cv2.THRESH_BINARY)
    #ret,thresh2 = cv2.threshold(img,127,255,cv2.THRESH_BINARY_INV)
    #ret,thresh3 = cv2.threshold(img,127,255,cv2.THRESH_TRUNC)
    #ret,thresh4 = cv2.threshold(img,127,255,cv2.THRESH_TOZERO)
    #ret,thresh5 = cv2.threshold(img,127,255,cv2.THRESH_TOZERO_INV)

    ret, gray_thresh = cv.threshold(gray,127,255,cv.THRESH_BINARY_INV)

    return gray_thresh


def generate_masks(image_path, save_path, image_size, limit, files_count=None):
    print('Images:', image_path, 'Save to:', save_path)
    H = image_size[0]
    W = image_size[1]
    total_pixels_image = W*H
            
    #sys.path.insert(0,'..')

    imageDir = image_path

    #valid_image_extensions = [".jpg", ".jpeg", ".png", ".tif", ".tiff"]
    #valid_image_extensions = [item.lower() for item in valid_image_extensions]

    file_extension = '.jpg'
    
    filenames = []
    files_readed = 0

    image1 = None
    image2 = None
    
    import glob
    
    for file in glob.iglob(data_path + '*' + file_extension):
        print('Reading', files_readed, file)
        files_readed += 1
        
        image2 = cv.imread(file)
        if ((image1 is not None) and (image2 is not None)):

            mask = get_mask(image1, image2)
            total_pixels_mask = sum(sum(mask))
            rel = total_pixels_mask / total_pixels_image
            #print(rel)
            if (mask is not None) and (rel >= limit[0] and rel <=limit[1]):
                mask_path = save_path+'mask_'+str(files_readed)
                print(files_readed, "writing:", mask_path)
                mask_3 = np.array([ [ [ v, v, v ] for v in r ] for r in mask ])
                cv.imwrite(mask_path+file_extension, mask_3)

                if files_count is not None and files_count >= files_readed:
                    return
                
        elif image2 is None:
            print ("Error loading: " + imagePath)
            continue

        image1 = image2
    
    
if __name__ == '__main__':

    
    data_path = 'small/val_256/'
    save_path = 'small/masks_256_tmp/'

    if not os.path.exists(save_path):
        os.makedirs(save_path)
        
    image_size = [256,256]
    limit = [0.03, 0.3] # 
    files_count = 1000
    generate_masks(data_path, save_path, image_size, limit)
