import numpy as np
import keras.backend as K
import tensorflow as tf
from utils import gram_mat, calc_dilation, calc_I_comp, roll
from keras.applications import VGG16
from keras.layers import Concatenate
from vgg_interface import VGG16Frozen

def hole_loss(M, I_out, I_gt):
    diff = I_out - I_gt
    return tf.norm((1 - M) * diff, ord=1)

def valid_loss(M, I_out, I_gt):
    #print("shapes M, out gt", M, I_out, I_gt)
    diff = I_out - I_gt
    return tf.norm(M * diff, ord=1)

def perceptual_loss(activ_on_out, activ_on_comp, activ_on_gt):
    loss = 0
    for i in range(len(activ_on_out)):
        loss += tf.norm(activ_on_out[i] - activ_on_gt[i], ord=1) \
            + tf.norm(activ_on_comp[i] - activ_on_gt[i], ord=1)
    return loss

def style_loss(activ_on_style, activ_on_gt):
    loss = tf.Variable(0.)
    for i in range(len(activ_on_style)):
        O = activ_on_style[i]
        G = activ_on_gt[i] 
        # TODO: check the correctness of batch calculation here!
        # this is a kind of tensorflow way to calculate
        # bn, hn, wh, cn = O.shape
        norm_coef = tf.reduce_prod(tf.cast(tf.shape(O), dtype=tf.float32))
        loss.assign_add(tf.norm(gram_mat(O) - gram_mat(G), ord=1) / norm_coef)
    return loss

def tv_loss(I_comp, P):
    #print("tv_loss")
    #print("shape I_comp", I_comp.shape)
    #print("shape P", P.shape)
    # P here is a 1-pixel dilaton of initial binary mask M
    mask = (1 - P)
    left_shift = roll(I_comp, shift=-1, axis=1)
    left_shited_mask = roll(mask, shift=-1, axis=1)#, wrap=False)
    left_mask = left_shited_mask * mask

    up_shift = roll(I_comp, shift=-1, axis=0)
    up_shifted_mask = roll(mask, shift=-1, axis=0)#, wrap=False)
    up_mask = up_shifted_mask * mask

    return tf.norm(left_mask * (left_shift - I_comp), ord=1) + \
        tf.norm(up_mask * (up_shift - I_comp), ord=1)

def total_loss(y_true, y_pred):
    I_out = y_pred[:,:,:,0:3]
    M = y_pred[:,:,:,3:6]
    #I_out = y_pred[0]
    #M = y_pred[1]
    #print('TYPE:', type(y_true))
    y_true = tf.reshape(y_true, tf.shape(I_out))
    I_gt = y_true
    
    I_comp = tf.convert_to_tensor(calc_I_comp(I_out, I_gt, M), dtype=tf.float32)

    print("shape I_out!:", I_out.shape)
    print("shape I_comp!:", I_comp.shape)
    print("shape I_gt!:", I_gt.shape)

    '''
    vgg_on_out = VGG16Frozen(I_out)
    activ_on_out = vgg_on_out.outputs
    vgg_on_comp = VGG16Frozen(I_comp)
    activ_on_comp = vgg_on_comp.outputs
    vgg_on_gt = VGG16Frozen(I_gt)
    activ_on_gt = vgg_on_gt.outputs
    ''' 

    # TODO: rewrite in more pythonic way
    vgg_outputs = VGG16Frozen(Concatenate(axis=0)([I_out, I_comp, I_gt])).outputs
    activ_on_out = []
    activ_on_comp = []
    activ_on_gt = []
    for _, vgg_output in enumerate(vgg_outputs):
        out, comp, gt = tf.split(vgg_output, 3)
        activ_on_out.append(out)
        activ_on_comp.append(comp)
        activ_on_gt.append(gt)

    #print("shape activ_on_out", np.shape(activ_on_out))
    #print("shape activ_on_comp", np.shape(activ_on_comp))
    #print("shape activ_on_gt", np.shape(activ_on_gt))
    
    valid = valid_loss(M, I_out, I_gt)
    hole = hole_loss(M, I_out, I_gt)
    perceptual = perceptual_loss(activ_on_out, activ_on_comp, activ_on_gt)
    style_out = style_loss(activ_on_out, activ_on_gt)
    style_comp = style_loss(activ_on_comp, activ_on_gt)
    tv = tv_loss(I_comp, calc_dilation(M))#, mode = 'cross', inverted=False))

    return valid + 6*hole + 0.05*perceptual + 120*(style_out + style_comp) + 0.1*tv
