import os 
import numpy as np 
from scipy import misc 
import tensorflow as tf 
from keras.applications import VGG16 
from keras.callbacks import ModelCheckpoint, LearningRateScheduler 
from keras.models import load_model

try:
    import unet
    from data_generator import DataGeneratorStream
    from vgg_interface import VGG16Frozen
except ImportError:  
    import sys
    sys.path.insert(0,'..')
    import unet
    from data_generator import DataGeneratorStream
    from vgg_interface import VGG16Frozen
    
from utils import get_activations

from skimage.transform import resize

# TODO: rewrite as unit test
def save_activations(model, img_path, layer_names):
    img = misc.imread(img_path)

    activations = get_activations(model, img, layer_names)
    
    for name, activ in zip(layer_names, activations):
        dir = os.path.join('.', 'activations', name)
        if not os.path.exists(dir):
            os.makedirs(dir)
        for ind in range(0, activ.shape[-1]):
            misc.imsave('{}.jpg'.format(os.path.join(dir, str(ind))), activ[0,:,:,ind])

def vgg_activations():
    model = VGG16()
    model.add_loss()
    save_activations(model, './data/dog224.jpg', ['block1_pool', 'block2_pool', 'block3_pool'])


def get_data(data_path, files_to_read=None):
    print("Get data from:", data_path)
    images = []
    
    valid_image_extensions = [".jpg", ".jpeg", ".png", ".tif", ".tiff"]
    valid_image_extensions = [item.lower() for item in valid_image_extensions]
    
    files_readed = 0 # -- TEMP --
    #files = os.listdir(data_path)
    #print('Reading', files_to_read, 'from total', len(files), 'files')
    #for file in files:
        
        #extension = os.path.splitext(file)[1]
        #if extension.lower() not in valid_image_extensions:
        #    continue

    import glob

    for file in glob.iglob(data_path + '/*.jpg'):
        print('Reading', files_readed, file)    
        #img_path = os.path.join(data_path, file)
        
        img = misc.imread(file)
        
        # -- TEMP --
        # upscale x2
        img = resize(img, (512, 512))
        
        #print(len(img),len(img[0]),len(img[0][0]))
        
        if img is not None:
            images.append(img)
            
        files_readed += 1
        
        if files_to_read is not None and files_readed >= files_to_read:
            break
        
        print("files readed", files_readed)
    return images


def train(train_gen, val_gen):
    print("Creaete checkpoint")
    model_checkpoint = ModelCheckpoint('unet_inpainting.hdf5', monitor='loss', verbose=1, save_best_only=True)
    # init model
    if os.path.isfile('unet_inpainting_prev.hdf5'):
        print("Loading previous model...")
        test_model  = load_model('unet_inpainting_prev.hdf5', custom_objects={"total_loss": total_loss})
    else:
        print("Building new model...")
        test_model = unet.UnetInpainting()
        test_model.build_model()
        test_model = test_model.unet_model
    print('Fitting model...')
    test_model.fit_generator(generator=train_gen,
                            validation_data=val_gen,
                            use_multiprocessing=True,
                            workers=8,
                            verbose=1,
                            steps_per_epoch=1000,
                            epochs=1000,
                            callbacks = [model_checkpoint])


def test_model(dataset_path, model_name, images_batch, masks_batch):

    from losses import total_loss

    print("Getting model")
    test_model  = load_model(dataset_path + model_name, custom_objects={"total_loss": total_loss})
    print("Predicting")
    import time
    start_time = time.time()
    result = test_model.predict([images_batch, masks_batch])
    print('Prediction took: ', time.time() - start_time)
    print("Writing results")
    dir = os.path.join('.', 'activations', 'prediction', model_name)
    for activ in result:
        if not os.path.exists(dir):
            os.makedirs(dir)
        for ind in range(0, activ.shape[-1]):
            misc.imsave('{}.jpg'.format(os.path.join(dir, str(ind))), activ[:,:,ind])

        # TODO save rgb if there are more than one image...
        misc.imsave('{}.jpg'.format(os.path.join(dir, 'rgb')), activ[:,:,0:3])


def get_model_memory_usage(batch_size, model):
    import numpy as np
    from keras import backend as K

    shapes_mem_count = 0
    for l in model.layers:
        single_layer_mem = 1
        for s in l.output_shape:
            if s is None:
                continue
            single_layer_mem *= s
        shapes_mem_count += single_layer_mem

    trainable_count = np.sum([K.count_params(p) for p in set(model.trainable_weights)])
    non_trainable_count = np.sum([K.count_params(p) for p in set(model.non_trainable_weights)])

    total_memory = 4.0*batch_size*(shapes_mem_count + trainable_count + non_trainable_count)
    gbytes = np.round(total_memory / (1024.0 ** 3), 3)
    return gbytes
        
if __name__ == "__main__":

    dataset_path = './data/'

    ##train_dir = 'data_256' # TODO: read images from subdirs
    train_dir = os.path.join(dataset_path, 'train')
    validation_dir = os.path.join(dataset_path, 'val')
    test_dir = os.path.join(dataset_path, 'test')
    mask_dir = os.path.join(dataset_path, 'masks')

    # num_files = 1

    # test memory usage:
    '''
    test_model = unet.UnetInpainting()
    test_model.build_model()
    vgg16 = VGG16Frozen(tf.constant(0., shape=(4,512,512,3), dtype=tf.float32))
    print('Needed amount of memory for:')
    print('UNet part:', get_model_memory_usage(2, test_model.unet_model))
    print('VGG part:', get_model_memory_usage(2, vgg16.model))
    exit()
    '''

    # test reading images
    print("Creating generators...")
    training_generator = DataGeneratorStream(train_dir, mask_dir)
    # TODO: check if masks are really needed for validation phase
    validation_generator = DataGeneratorStream(validation_dir, mask_dir)
    
    # print("Start training phase...")
    # train(training_generator, validation_generator)

    model_name = 'unet_inpainting_last.hdf5'
    imgs = misc.imread('./data/dog224.jpg')
    imgs = resize(imgs, (512, 512))
    images_batch = np.array([imgs])

    masks = misc.imread('./data/masks/mask_00000152.jpg')
    masks = resize(masks, (512, 512))
    # masks = np.array([ [ [ v, v, v ] for v in r ] for r in masks ])

    # misc.imsave('./holes_3.jpg', imgs * masks)

    masks_batch = np.array([masks])
    test_model(dataset_path, model_name, images_batch, masks_batch)
    
    
