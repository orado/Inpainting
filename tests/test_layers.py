import unittest
import numpy as np
import keras.backend as K

import tensorflow as tf

from keras.layers import Input, Lambda, multiply
from keras.models import Model

from utils import unit_step_activation, normalize, mask_scale

class LayersTest(unittest.TestCase):
    
    def test_reduce_sum_layer(self):
        from scipy import misc
        import numpy as np
        import os
        
        img_path = './data/test_net_model/'
        img_name = 'test_64x64.bmp'
        rgb = np.array([misc.imread(os.path.join(img_path, img_name))])
        
        # by default rgb is uint8, if overflows with summation, float32 rewuired for scalar_mul
        sum_rgb = tf.cast(rgb, tf.float32)
        axis = 3
        sum_rgb = tf.reduce_sum(sum_rgb, axis)
        sum_rgb = tf.scalar_mul(0.3, sum_rgb)
        sum_rgb = tf.cast(sum_rgb, tf.int32)

        sum_rgb_arr = tf.Session().run(sum_rgb)[0].tolist()
        
        if not os.path.exists(img_path):
            os.makedirs(img_path)
        
        misc.imsave('{}.bmp'.format(os.path.join(img_path, 'reduce_sum')), sum_rgb_arr)
        

    def test_multiply_layer(self):
        a = np.array([[[1, 2], [3, 4]]])
        b = np.array([[[5, 6], [7, 8]]])
        groud_truth = [[[5., 12.], [21., 32.]]]
        layer_in_a = Input(shape=(2,2))
        layer_in_b = Input(shape=(2,2))
        layer_out  = multiply([layer_in_a, layer_in_b])
        
        model = Model(inputs = [layer_in_a, layer_in_b], outputs = layer_out)
        result = model.predict([a, b])
        
        for r, g in zip(result[0], groud_truth[0]):
            for a,b in zip(r,g):
                self.assertEqual(a, b)


    def test_lambda_ceil_layer(self):
        a = np.array([[[0.1, 0.2], [0.0, 0.4]],[[0.5, 0.0], [0.0, 0.8]]])
        groud_truth =[[[1.0, 1.0], [0.0, 1.0]],[[1.0, 0.0], [0.0, 1.0]]]
        layer_in_a = Input(shape=(2,2))        
        layer_out  = Lambda(unit_step_activation)(layer_in_a)
        
        model = Model(inputs = layer_in_a, outputs = layer_out)  
        result = model.predict(a)
        
        for r, g in zip(result[0], groud_truth[0]):
            for a,b in zip(r,g):
                self.assertEqual(a, b)

                
    def test_lambda_normalize_layer(self):
        a = np.array([[[1.0, 2.0], [3.0, 4.0]],[[5.0, 6.0], [7.0, 8.0]]])
        groud_truth =[[[0.0, 0.14285],[0.28571, 0.42857]],[[0.57142, 0.71428],[0.85714, 1.]]]
        layer_in_a = Input(shape=(2,2))
        layer_out  = Lambda(normalize)(layer_in_a)
        
        model = Model(inputs = layer_in_a, outputs = layer_out)  
        result = model.predict(a)

        for r, g in zip(result[0], groud_truth[0]):
            for a,b in zip(r,g):
                self.assertAlmostEqual(a, b, 4)

    def test_lambda_mask_scale_layer(self):
        a = np.array([[[1.0, 2.0], [3.0, 4.0]],[[5.0, 6.0], [7.0, 8.0]]])
        b = np.array([[[1.0, 1.0], [0.0, 1.0]],[[1.0, 0.0], [0.0, 1.0]]])
        groud_truth =[[[0.2, 0.4], [0.6, 0.8]],[[1.0, 1.2], [1.4, 1.6]]]
        layer_in_a = Input(shape=(2,2))
        layer_in_b = Input(shape=(2,2)) 
        layer_out  = Lambda(mask_scale)([layer_in_a, layer_in_b])
        
        model = Model(inputs = [layer_in_a, layer_in_b], outputs = layer_out)  
        result = model.predict([a, b])

        for r, g in zip(result[0], groud_truth[0]):
            for a,b in zip(r,g):
                self.assertAlmostEqual(a, b, 1)