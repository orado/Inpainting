import unittest
import numpy as np
import keras.backend as K
import losses
import utils

class LossTest(unittest.TestCase):

    def test_hole_loss(self):
        M = K.eye(3)
        I_out = K.ones((3, 3))
        I_gt = K.zeros((3,3))
        self.assertEqual(K.eval(losses.hole_loss(M, I_out, I_gt)), 6)

    def test_valid_loss(self):
        M = K.eye(3)
        I_out = K.ones((3, 3))
        I_gt = K.zeros((3,3))
        self.assertEqual(K.eval(losses.valid_loss(M, I_out, I_gt)), 3)

    def test_perceptual_loss_small(self):
        activ_on_out = np.array([ K.ones((3,3)), K.eye(3), K.zeros((3,3)) ])
        activ_on_comp = np.array([ K.zeros((3,3)), K.ones((3,3)), K.eye(3) ])
        activ_on_gt = np.array([ K.eye(3), K.zeros((3,3)), K.ones((3,3)) ])
        self.assertEqual(K.eval(losses.perceptual_loss(activ_on_out, activ_on_comp ,activ_on_gt)), 36)

    def test_perceptual_loss_medium(self):
        shape1 = (112,112,64)
        activ_on_out = np.array([ K.ones(shape1) ])
        activ_on_comp = np.array([ K.zeros(shape1) ])
        activ_on_gt = np.array([ K.variable(np.stack([np.eye(112)] * 64, axis=-1)) ])

        h,w,c = shape1
        expected_loss = np.abs(h*w*c - w*c) + np.abs(0 - w*c)
        self.assertEqual(K.eval(losses.perceptual_loss(activ_on_out, activ_on_comp ,activ_on_gt)), expected_loss)

    def test_perceptual_loss_large(self):
        # lets test on the real shapes of activations from VGG-16
        shape1 = (112,112,64)
        shape2 = (56,56,128)
        shape3 = (28,28,256)
        activ_on_out = np.array([ K.ones(shape1),
                                    K.variable(np.stack([np.eye(56)] * 128, axis=-1)),
                                    K.zeros(shape3) ])
        activ_on_comp = np.array([ K.zeros(shape1),
                                    K.ones(shape2),
                                    K.variable(np.stack([np.eye(28)] * 256, axis=-1)) ])
        activ_on_gt = np.array([ K.variable(np.stack([np.eye(112)] * 64, axis=-1)),
                                    K.zeros(shape2),
                                    K.ones(shape3) ])

        h1,w1,c1 = shape1
        h2,w2,c2 = shape2
        h3,w3,c3 = shape3
        expected_loss = np.abs(h1*w1*c1 - w1*c1) + np.abs(0 - w1*c1) \
            + np.abs(w2*c2 - 0) + np.abs(h2*w2*c2 - 0) \
            + np.abs(0 - h3*w3*c3) + np.abs(w3*c3 - h3*w3*c3)
        self.assertEqual(K.eval(losses.perceptual_loss(activ_on_out, activ_on_comp ,activ_on_gt)), expected_loss)


    def test_tv_loss(self):
        M = np.ones((3, 3))
        M[1, 1] = 0
        P = utils.calc_dilation(M, 'cross')
        # TODO: decide what to do with default floating-point type inconsistency
        # in numpy it's float64 but in Keras - float32
        I_comp = K.variable([[1, 2, 3], [4, 5, 6], [7, 8, 9]], dtype=np.float64)
        self.assertEqual(K.eval(losses.tv_loss(I_comp, P)), 8)


    def test_total_loss_same_img_zero_mask(self):
        image = np.random.randint(256, size=(512,512,3)).astype(np.float32)
        mask = np.ones((512,512,3), dtype=np.float32)
        
        images = np.array([image])
        masks = np.array([mask])

        output = [images, masks]

        self.assertEqual(K.eval(losses.total_loss(images, output)), 0)


    def test_total_loss_same_img(self):
        image = np.random.randint(256, size=(512,512,3)).astype(np.float32)
        mask = np.random.randint(2, size=(512,512,3)).astype(np.float32)
        
        images = np.array([image])
        masks = np.array([mask])

        output = [images, masks]

        P = utils.calc_dilation(masks, 'cross')
        expected_loss = 0.1 * K.eval(losses.tv_loss(images, P))

        self.assertAlmostEqual(K.eval(losses.total_loss(images, output)), expected_loss, 0)