class VGG16Frozen():
    def __init__(self, input_tensor, input_shape=(512,512,3)):
        from keras.models import Model
        from keras.layers import Conv2D, MaxPooling2D, Input
        from keras.utils import data_utils

        input_tensor = Input(tensor=input_tensor)
        # Determine proper input shape
        # Block 1
        x = Conv2D(64, (3, 3), activation='relu', padding='same', name='block1_conv1', trainable=False)(input_tensor)
        x = Conv2D(64, (3, 3), activation='relu', padding='same', name='block1_conv2', trainable=False)(x)
        self.block1_pool = MaxPooling2D((2, 2), strides=(2, 2), name='block1_pool', trainable=False)(x)

        # Block 2
        x = Conv2D(128, (3, 3), activation='relu', padding='same', name='block2_conv1', trainable=False)(self.block1_pool)
        x = Conv2D(128, (3, 3), activation='relu', padding='same', name='block2_conv2', trainable=False)(x)
        self.block2_pool = MaxPooling2D((2, 2), strides=(2, 2), name='block2_pool', trainable=False)(x)

        # Block 3
        x = Conv2D(256, (3, 3), activation='relu', padding='same', name='block3_conv1', trainable=False)(self.block2_pool)
        x = Conv2D(256, (3, 3), activation='relu', padding='same', name='block3_conv2', trainable=False)(x)
        x = Conv2D(256, (3, 3), activation='relu', padding='same', name='block3_conv3', trainable=False)(x)
        self.block3_pool = MaxPooling2D((2, 2), strides=(2, 2), name='block3_pool', trainable=False)(x)

        self.outputs = [self.block1_pool, self.block2_pool, self.block3_pool]

        self.model = Model(input_tensor, outputs=self.outputs, name='vgg16')

        WEIGHTS_PATH_NO_TOP = 'https://github.com/fchollet/deep-learning-models/releases/download/v0.1/vgg16_weights_tf_dim_ordering_tf_kernels_notop.h5'

        weights_path = data_utils.get_file('vgg16_weights_tf_dim_ordering_tf_kernels_notop.h5',
                                    WEIGHTS_PATH_NO_TOP,
                                    cache_subdir='models',
                                    file_hash='6d6bbae143d832006294945121d1f1fc')
        self.model.load_weights(weights_path, by_name=True)

